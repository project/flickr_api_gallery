<?php

namespace Drupal\flickr_api_gallery\Service;

use Drupal\flickr_api\Service\Photosets;

/**
 * Prepare albums data
 */
class PhotosetDataLoader {
    protected Photosets $photosets;

    public function __construct(Photosets $photosets) {
        $this->photosets = $photosets;
    }

    /**
     * Returns albums data
     */
    public function getPhotosetData(String $album_id, int $albums_per_page, int $active_page, string $url,String $flickr_id) {
        //$photosets = $this->photosets->photosetsGetList($flickr_id, $active_page, $albums_per_page, false, true);
        $photoset = $this->photosets->photosetsGetPhotos($album_id, ['per_page' => $albums_per_page], $active_page, false);
        $photoset_info = $this->photosets->photosetsGetInfo($album_id);
        $album = [];

        // If the required page index was out of range create new request with the latest page number
        if (count($photoset['photo']) == 0) {
            $active_page = $photoset['pages'];
            $photoset = $this->photosets->photosetsGetPhotos($album_id, ['per_page' => $albums_per_page], $active_page, false);
        }

        foreach ($photoset['photo'] as $photo) {
            $index = count($album);

            $album[$index] = [
                'id' => $photo['id'],
                'thumbnailURL' => $photo['url_m'],
                'thumbnailWidth' => $photo['width_m'],
                'thumbnailHeight' => $photo['height_m'],
                'altTag' => $photo['title'],
                'largeUrl' => $this->getPhotoLargeURL($photo),
            ];
        }

        return [
            'photos' => $album,
            'pager' => $this->getPager($url, $photoset['page'], $photoset['pages']),
            'title' => $photoset['title'],
            'siblings' => $this->getPhotosetSiblings($flickr_id, $album_id),
            'description' => $photoset_info['description']['_content'],
            'background_image' => $this->generateBackgrounImage($photoset_info),
            'cache_tags' => ['photoset_page', $album_id, $active_page]
        ];
    }


    private function getPhotoThumbnailURL($photo) {
        $size='w';
        $img = 'https://farm'.$photo['farm'].'.staticflickr.com/'.$photo['server'].'/'.$photo['id'].'_'.$photo['secret'].'_'.$size.'.jpg';

        return $img;
    }

    private function getPhotoLargeURL($photo) {
        $size='b';
        $img = 'https://farm'.$photo['farm'].'.staticflickr.com/'.$photo['server'].'/'.$photo['id'].'_'.$photo['secret'].'_'.$size.'.jpg';

        return $img;
    }

    private function getPager(String $url, int $active_page, int $pages_count) {
        $pagination = [];

        for ($i = 1; $i <= $pages_count ; $i++) {
            $pagination[$i] = [
                'index' => $i,
                'url' => $url . '?page=' . $i,
                'active' => $active_page == $i,
            ];
        }

        return $pagination;
    }

    private function getPhotosetSiblings(String $flickr_id, int $album_id) {
        $photosets = $this->photosets->photosetsGetList($flickr_id);

        $albumPosition = intval(key(array_filter($photosets, function($album) use ($album_id) {
            return $album['id'] == $album_id;
        })));

        $previousAlbum = $albumPosition != 0 ? $photosets[$albumPosition - 1] : null;
        $nextAlbum = $albumPosition < count($photosets) ? $photosets[$albumPosition + 1] : null;

        return [
            'previousAlbum' => $previousAlbum,
            'nextAlbum'=> $nextAlbum
        ];
    }

    private function generateBackgrounImage(array $album) {
        return 'https://farm' . $album['farm'] . '.staticflickr.com//' . $album['server'] . '//' . $album['primary'] . '_' . $album['secret'] . '_b' . '.jpg';
    }
}
