<?php

namespace Drupal\flickr_api_gallery\Service;

use Drupal\flickr_api\Service\Photosets;

/**
 * Prepare albums data
 */
class AlbumsDataLoader {
    protected Photosets $photosets;

    public function __construct(Photosets $photosets) {
        $this->photosets = $photosets;
    }

    /**
     * Returns albums data
     */
    public function getAlbumsData(String $flickr_id, int $albums_per_page,int $active_page, String $url) {

        $photosets = $this->photosets->photosetsGetList($flickr_id, $active_page, $albums_per_page, false, true);

        // If the required page index was out of range create new request with the latest page number
        if (count($photosets['photoset']) == 0) {
            $active_page = $photosets['pages'];
            $photosets = $this->photosets->photosetsGetList($flickr_id, $active_page, $albums_per_page, true, true);
        }
        $albums = [];

        foreach ($photosets['photoset'] as $photoset) {
            $index = count($albums);

            $albums[$index] = [
                'id' => $photoset['id'],
                'title' => $photoset['title']['_content'],
                'count_photos' => $photoset['count_photos'],
                'cover_image' => $this->getAlbumCoverImageURL($photoset),
            ];
        }

        return [
            'albums' => $albums,
            'pages_count' => $photosets['pages'],
            'per_page' => $photosets['perpage'],
            'total_albums' => $photosets['total'],
            'pager' => $this->getPager($url, $active_page, $photosets['pages']),
            'cache_tags' => ['albums_page', $active_page, $photosets['photoset'][0]['id']],
        ];
    }

    private function getAlbumCoverImageURL($album) {
        $size='w';
        $img = 'https://farm'.$album['farm'].'.staticflickr.com/'.$album['server'].'/'.$album['primary'].'_'.$album['secret'].'_'.$size.'.jpg';

        return $img;
    }

    private function getPager(String $url, int $active_page, int $pages_count) {
        $pagination = [];

        for ($i = 1; $i <= $pages_count ; $i++) {
            $pagination[$i] = [
                'index' => $i,
                'url' => $url . '?page=' . $i,
                'active' => $active_page == $i,
            ];
        }

        return $pagination;
    }
}
