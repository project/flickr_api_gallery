<?php

namespace Drupal\flickr_api_gallery\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\flickr_api_gallery\Service\PhotosetDataLoader;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\flickr_api_gallery\Service\AlbumsDataLoader;
use Symfony\Component\HttpFoundation\Request;

/**
* Implements ControllerBase
*/
class FlickrApiGalleryController extends ControllerBase{
    protected AlbumsDataLoader $albums;
    protected PhotosetDataLoader $photoset;

    public function __construct(AlbumsDataLoader $photosets, PhotosetDataLoader $photos) {
        $this->albums = $photosets;
        $this->photoset = $photos;
    }

    public static function create(ContainerInterface $container) {
        return new static($container->get('flickr_api_gallery.albums_data'), $container->get('flickr_api_gallery.photoset_data'));
    }

    public function albumsPage(string $album_id, Request $request) {
        $config = $this->config('flickr_api_gallery.configuration');

        $page = intval($request->query->get('page'));
        if ($page == NULL || !is_int($page) || $page < 1) {
            $page = 1;
        }

        $url = $this->generateURL($request->getUri(), $request->getPathInfo());

        if ($album_id == '0') {
            $response = $this->loadAlbums($config->get('flickr_id'), $config->get('albums_per_page'), $page, $url, $config->get('seo_description'));
        } else {
            $response = $this->loadPhotoset($album_id, $config->get('photos_per_page'), $page, $url, $config->get('flickr_id'));
        }

		return $response;
	}

    private function generateURL(String $url, String $path): string
    {
        $url = explode($path, $url);

        return $url[0] . $path;
    }

    private function loadAlbums(string $flickr_id,int $albums_per_page,int $page,string $url, string $seo_description) {
        $albums = $this->albums->getAlbumsData($flickr_id, $albums_per_page, $page, $url);

        $description = [
            '#tag' => 'meta',
            '#attributes' => [
                'name' => 'description',
                'content' => $seo_description,
            ]
        ];

        return [
            '#theme' => 'albums_page',
            '#cache' => [
                'keys' => $albums['cache_tags']
                //'max-age' => 0
            ],
            '#attached' => [
                'library' => [
                    'flickr_api_gallery/albums-page'
                ],
                'html_head' => [[$description,'description']]
            ],
            '#albums' => $albums['albums'],
            '#albums_per_page' => $albums_per_page,
            '#pager' => $albums['pager'],
            '#url' => $url,
        ];
    }

    private function loadPhotoset(string $album_id, int $photos_per_page, int $page, string $url,String $flickr_id) {
        $photoset = $this->photoset->getPhotosetData($album_id, $photos_per_page, $page, $url, $flickr_id);

        return [
            '#theme' => 'photoset_page',
            '#attached' => [
                'library' => [
                    'flickr_api_gallery/photoset-page',
                    'flickr_api_gallery/justified-gallery',
                    'flickr_api_gallery/light-gallery',
                ]
            ],
            '#cache' => [
                'keys' => $photoset['cache_tags'],
                //'max-age' => 0
            ],
            '#photos' => $photoset['photos'],
            '#pager' => $photoset['pager'],
            '#url' => $url,
            '#base_url' => $this::generateBaseURL($url),
            '#title' => $photoset['title'],
            '#siblings' => $photoset['siblings'],
            '#description' => $photoset['description'],
            '#background_image' => $photoset['background_image'],
        ];
    }

    private function generateBaseURL(string $url) {
        $album_id = '/' . \Drupal::routeMatch()->getParameter('album_id');
        $url = explode($album_id, $url);

        return $url[0];
    }
}
