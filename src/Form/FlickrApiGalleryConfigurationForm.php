<?php

namespace Drupal\flickr_api_gallery\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configuration form definition for the salutation message.
 */
class FlickrApiGalleryConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['flickr_api_gallery.configuration'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'flickr_api_gallery_configuration';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('flickr_api_gallery.configuration');

    $form['flickr_id'] = array(
        '#type' => 'textfield',
        '#title' => $this->t('Flickr ID'),
        '#description' => $this->t('Please provide the Flickr ID you want to use.'),
        '#default_value' => $config->get('flickr_id'),
    );
    $form['albums_per_page'] = array(
        '#type' => 'number',
        '#title' => $this->t('Galleries number per page'),
        '#description' => $this->t('Please provide how many galleries you want to have on main page.'),
        '#default_value' => $config->get('albums_per_page'),
    );
      $form['photos_per_page'] = array(
          '#type' => 'number',
          '#title' => $this->t('Photos number per page on photoset page'),
          '#description' => $this->t('Please provide how many photos you want to have on photoset (album) page.'),
          '#default_value' => $config->get('photos_per_page'),
      );
      $form['seo_description'] = array(
          '#type' => 'textarea',
          '#title' => $this->t('SEO description'),
          '#description' => $this->t('Provide SEO description for main galleries page.'),
          '#default_value' => $config->get('seo_description'),
      );

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('flickr_api_gallery.configuration')
      ->set('flickr_id', $form_state->getValue('flickr_id'))
      ->set('albums_per_page', $form_state->getValue('albums_per_page'))
      ->set('photos_per_page', $form_state->getValue('photos_per_page'))
      ->set('seo_description', $form_state->getValue('seo_description'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
