
(function ($) {
    $(function() {
        $("#customAlbum").justifiedGallery({
            rowHeight : 220,
            maxRowHeight: '200%',
            margins : 3,
            lastRow: 'center',
            cssAnimation: true,
            waitThumbnailsLoad: false,
            captions: false,
        }).on('jg.complete', function (e) {
            lightGallery(document.getElementById('customAlbum'), {
                plugins: [lgZoom, lgAutoplay, lgFullscreen, lgHash, lgRotate, lgShare],
                speed: 500,
                pinterest: false,
                flipHorizontal: false,
                flipVertical: false,
                customSlideName: false,
            });
        });
    });
}(jQuery));

