
(function ($) {
    $(function() {
        $("#customAlbum").justifiedGallery({
            rowHeight : 220,
            maxRowHeight: '200%',
            margins : 3,
            lastRow: 'center',
            cssAnimation: true,
            waitThumbnailsLoad: false,
            captions: false,
        });
    });
}(jQuery));

