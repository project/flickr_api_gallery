- [ ] Configure caching
- [ ] Configure seo attributes
- [ ] Ajax loading of images
- [ ] Add link to drupal configuration for this config


## Necessary patch
https://www.drupal.org/files/issues/2021-09-10/Add-option-to-return-all-in-photosetsGetList-method.patch
